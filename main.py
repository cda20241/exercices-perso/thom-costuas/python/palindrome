"""
Palindrome Checker

This script defines a function to check if a given string is a palindrome. A palindrome is a word, phrase, or sequence that reads the same backward as forward.

Functions:
- estPalindrome(chaineAEvaluer): Checks if the input string is a palindrome.

Usage:
Run the script and enter a string when prompted to check if it is a palindrome.

Example:
python script_name.py
"""

import unidecode

def estPalindrome(chaineAEvaluer):
    """
    Checks if a given string is a palindrome.

    Parameters:
    - chaineAEvaluer (str): The input string to be evaluated.

    Returns:
    - bool: True if the string is a palindrome, False otherwise.
    """
    inverseChaine = ""
    chaineAEvaluer = chaineAEvaluer.lower()
    chaineAEvaluer = unidecode.unidecode(chaineAEvaluer)
    chaineAEvaluer = str.replace(chaineAEvaluer, " ", "")

    for i in range(len(chaineAEvaluer) - 1, -1, -1):
        inverseChaine += chaineAEvaluer[i]

    if inverseChaine == chaineAEvaluer:
        return True
    else:
        return False

# Example usage of the function
chaine = input("Enter a string: ")

if estPalindrome(chaine):
    print("C'est palindrome")
else:
    print("Ce n'est pas un palindrome ")
